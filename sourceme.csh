#
# Copyright (c) 2018 10x Genomics, Inc. All rights reserved.
#
# Environment setup for package longranger-2.2.2.
# Source this file before running.
#

set SOURCE=($_)
if ( "$SOURCE" != "" ) then
    set SOURCE=`readlink -f "$SOURCE[2]"`
else
    set SOURCE=`readlink -f "$0"`
endif
set DIR=`dirname $SOURCE`

#
# Source user's own environment first.
#
# Note .login is for login shells only.
source ~/.cshrc

#
# Modify the prompt to indicate user is in 10X environment.
#


#
# Set aside environment variables if they may conflict with 10X environment
#

if ( ! $?_TENX_LD_LIBRARY_PATH ) then
    setenv _TENX_LD_LIBRARY_PATH "$LD_LIBRARY_PATH"
    setenv LD_LIBRARY_PATH ""
endif


#
# Unset environment variables if they may conflict with 10X environment
#

if ( $?PYTHONPATH ) then
    unsetenv PYTHONPATH
endif


#
# Add module binary paths to PATH
#

if ( ! $?PATH ) then
    setenv PATH "$DIR/anaconda-cs/2.2.0-anaconda-cs-c8/bin"
else
    setenv PATH "$DIR/anaconda-cs/2.2.0-anaconda-cs-c8/bin:$PATH"
endif
setenv PATH "$DIR/tiny-ref/2.1.0:$PATH"
setenv PATH "$DIR/vcflib/726b788:$PATH"
setenv PATH "$DIR/martian-cs/2.3.2/bin:$PATH"
setenv PATH "$DIR/samtools_new/1.6:$PATH"
setenv PATH "$DIR/bwa/0.7.12:$PATH"
setenv PATH "$DIR/tiny-fastq/2.0.0:$PATH"
setenv PATH "$DIR/bedtools/v2.26.0:$PATH"
setenv PATH "$DIR/longranger-cs/2.2.2/bin:$PATH"
setenv PATH "$DIR/longranger-cs/2.2.2/lib/bin:$PATH"
setenv PATH "$DIR/longranger-cs/2.2.2/tenkit/bin:$PATH"
setenv PATH "$DIR/longranger-cs/2.2.2/tenkit/lib/bin:$PATH"
setenv PATH "$DIR/bcftools/9058fceb15:$PATH"
setenv PATH "$DIR/freebayes/7dd41dbc8:$PATH"


#
# Module-specific env vars
#
# martian-cs

if ( ! $?PYTHONPATH ) then
    setenv PYTHONPATH "$DIR/martian-cs/2.3.2/adapters/python"
else
    setenv PYTHONPATH "$DIR/martian-cs/2.3.2/adapters/python:$PYTHONPATH"
endif
# longranger-cs
setenv MROFLAGS "--vdrmode=rolling"
setenv LC_ALL "C"
setenv MROPATH "$DIR/longranger-cs/2.2.2/mro"

if ( ! $?MROPATH ) then
    setenv MROPATH "$DIR/longranger-cs/2.2.2/lib/tada/mro"
else
    setenv MROPATH "$DIR/longranger-cs/2.2.2/lib/tada/mro:$MROPATH"
endif
setenv MROPATH "$DIR/longranger-cs/2.2.2/tenkit/mro:$MROPATH"
setenv PYTHONPATH "$DIR/longranger-cs/2.2.2/lib/python:$PYTHONPATH"
setenv PYTHONPATH "$DIR/longranger-cs/2.2.2/tenkit/lib/python:$PYTHONPATH"
setenv PYTHONUSERBASE "$DIR/longranger-cs/2.2.2/lib"

