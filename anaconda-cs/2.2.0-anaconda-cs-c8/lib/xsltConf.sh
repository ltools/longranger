#
# Configuration file for using the xslt library
#
XSLT_LIBDIR="-L/mnt/home/jenkins/workspace/anaconda-cs-2.2.0-lr-master-latest/sake/modules/anaconda-cs/2.2.0-anaconda-cs-c8/lib"
XSLT_LIBS="-lxslt  -L/mnt/home/jenkins/workspace/anaconda-cs-2.2.0-lr-master-latest/sake/modules/anaconda-cs/2.2.0-anaconda-cs-c8/lib -lxml2 -lz -lm -ldl -lm -lrt"
XSLT_INCLUDEDIR="-I/mnt/home/jenkins/workspace/anaconda-cs-2.2.0-lr-master-latest/sake/modules/anaconda-cs/2.2.0-anaconda-cs-c8/include"
MODULE_VERSION="xslt-1.1.28"
