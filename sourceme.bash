#
# Copyright (c) 2018 10x Genomics, Inc. All rights reserved.
#
# Environment setup for package longranger-2.2.2.
# Source this file before running.
#

# Determine path to this script; resolve symlinks
SOURCE="${BASH_SOURCE[0]}"
while [ -h "$SOURCE" ]; do
    DIR="$( cd -P "$( dirname "$SOURCE" )" > /dev/null && pwd )"
    SOURCE="$(readlink "$SOURCE")"
    [[ $SOURCE != /* ]] && SOURCE="$DIR/$SOURCE"
done
DIR="$( cd -P "$( dirname "$SOURCE" )" > /dev/null && pwd )"

#
# Source user's own environment first.
#
# Only source .bashrc if we're being sourced from shell10x script.
# Otherwise, we could end up in an infinite loop if user is
# sourcing this file from their .bashrc.
# Note: .bash_profile is for login shells only.
if [ ! -z $_RUN10X ] && [ -e ~/.bashrc ]; then
    source ~/.bashrc
fi

#
# Modify the prompt to indicate user is in 10X environment.
#
if [ ! -z $_10X_MODPROMPT ]; then
    ORIG_PROMPT=$PS1
    PREFIX=$TENX_PRODUCT
    if [ ! -z $_10XDEV_BRANCH_PROMPT ]; then
        PREFIX="10X:\`pushd $(echo $MROPATH | cut -d : -f1) > /dev/null;git rev-parse --abbrev-ref HEAD;popd > /dev/null\`"
    fi
    export PS1="\[\e[0;34m\]$PREFIX\[\e[m\]>$ORIG_PROMPT"
fi

#
# Set aside environment variables if they may conflict with 10X environment
#

if [ -z "$_TENX_LD_LIBRARY_PATH" ]; then
    export _TENX_LD_LIBRARY_PATH="$LD_LIBRARY_PATH"
    export LD_LIBRARY_PATH=""
fi


#
# Unset environment variables if they may conflict with 10X environment
#

if [ ! -z "$PYTHONPATH" ]; then
    unset PYTHONPATH
fi


#
# Add module binary paths to PATH
#

if [ -z "$PATH" ]; then
    export PATH="$DIR/anaconda-cs/2.2.0-anaconda-cs-c8/bin"
else
    export PATH="$DIR/anaconda-cs/2.2.0-anaconda-cs-c8/bin:$PATH"
fi
export PATH="$DIR/tiny-ref/2.1.0:$PATH"
export PATH="$DIR/vcflib/726b788:$PATH"
export PATH="$DIR/martian-cs/2.3.2/bin:$PATH"
export PATH="$DIR/samtools_new/1.6:$PATH"
export PATH="$DIR/bwa/0.7.12:$PATH"
export PATH="$DIR/tiny-fastq/2.0.0:$PATH"
export PATH="$DIR/bedtools/v2.26.0:$PATH"
export PATH="$DIR/longranger-cs/2.2.2/bin:$PATH"
export PATH="$DIR/longranger-cs/2.2.2/lib/bin:$PATH"
export PATH="$DIR/longranger-cs/2.2.2/tenkit/bin:$PATH"
export PATH="$DIR/longranger-cs/2.2.2/tenkit/lib/bin:$PATH"
export PATH="$DIR/bcftools/9058fceb15:$PATH"
export PATH="$DIR/freebayes/7dd41dbc8:$PATH"


#
# Module-specific env vars
#
# martian-cs

if [ -z "$PYTHONPATH" ]; then
    export PYTHONPATH="$DIR/martian-cs/2.3.2/adapters/python"
else
    export PYTHONPATH="$DIR/martian-cs/2.3.2/adapters/python:$PYTHONPATH"
fi
# longranger-cs
export MROFLAGS="--vdrmode=rolling"
export LC_ALL="C"
export MROPATH="$DIR/longranger-cs/2.2.2/mro"

if [ -z "$MROPATH" ]; then
    export MROPATH="$DIR/longranger-cs/2.2.2/lib/tada/mro"
else
    export MROPATH="$DIR/longranger-cs/2.2.2/lib/tada/mro:$MROPATH"
fi
export MROPATH="$DIR/longranger-cs/2.2.2/tenkit/mro:$MROPATH"
export PYTHONPATH="$DIR/longranger-cs/2.2.2/lib/python:$PYTHONPATH"
export PYTHONPATH="$DIR/longranger-cs/2.2.2/tenkit/lib/python:$PYTHONPATH"
export PYTHONUSERBASE="$DIR/longranger-cs/2.2.2/lib"

